Name:           perl-Sub-Name
Version:        0.27
Release:        2
Summary:        (Re)name a sub
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Sub-Name
Source0:        https://cpan.metacpan.org/authors/id/E/ET/ETHER/Sub-Name-%{version}.tar.gz

BuildRequires:  gcc coreutils, findutils, perl-interpreter, perl-devel, perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker), perl(Exporter) >= 5.57, perl(strict)
BuildRequires:  perl(warnings), perl(XSLoader), perl(B), perl(B::Deparse), perl(Carp)
BuildRequires:  perl(feature), perl(File::Spec), perl(if), perl(Test::More) >= 0.88
BuildRequires:  perl(CPAN::Meta) >= 2.120900

%description
This module has only one function, which is also exported by default:
subname NAME, CODEREF

Assigns a new name to referenced sub. If package specification is
omitted in the name, then the current package is used. The return
value is the sub.

The name is only used for informative routines (caller, Carp, etc).
You won't be able to actually invoke the sub by the given name.
To allow that, you need to do glob-assignment yourself.

Note that for anonymous closures (subs that reference lexicals declared
outside the sub itself) you can name each instance of the closure differently,
which can be very useful for debugging.

%package_help

%prep
%autosetup -n Sub-Name-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor optimize="%{optflags}"
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT NO_PACKLIST=1
%{_fixperms} $RPM_BUILD_ROOT

%check
make test

%files
%license LICENCE
%doc CONTRIBUTING README
%{perl_vendorarch}/auto/Sub/
%{perl_vendorarch}/Sub/

%files help
%{_mandir}/man*/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.27-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue May 30 2023 Ge Wang <wang__ge@126.com> - 0.27-1
- Upgrade to version 0.27

* Sat Dec 24 2022 wanglimin <wanglimin@xfusion.com> - 0.26-2
- Fix date error

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.26-1
- Upgrade to version 0.26

* Wed 02 Jun 2021 zhaoyao<zhaoyao32@huawei.com> - 0.21-11
- fixs faileds: /bin/sh: gcc: command not found.

* Tue Jan 14 2020 hy-euler <eulerstoragemt@huawei.com> 0.21-10
- Delete the unnecessary BuildRequires

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.21-9
- Package init
